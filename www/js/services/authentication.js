var app = angular.module('mealtrack.services.authentication', []);

app.service('AuthService', function ($q, $ionicPopup) {
	var self = {
		user: Parse.User.current(),
		login: function (email, password) {
			var d = $q.defer();

      Parse.User.logIn(email, password, {
        success: function (user) {
          console.log(user);

          self.user = user;
          d.resolve(user);
        },
        error: function (user, err) {
          $ionicPopup.alert({
            title: 'Login Error!',
            subTitle: err.message
          });
          d.reject(err);
        }
      });

			return d.promise;
		},

		signup: function (email, name, password) {
			var d = $q.defer();

      var user = new Parse.User();
      user.set('username', email);
      user.set('email', email);
      user.set('name', name);
      user.set('password', password);

      user.signUp(null, {
        success: function (user) {
          console.log(user);
          self.user = user;
          d.resolve(user);
        },
        error: function (user, err) {
          $ionicPopup.error({
            title: 'Signup Error!',
            subTitle: err.message
          });
          d.reject(err);
        }
      });

			return d.promise;
		},

		'update': function (data)  {
			var d = $q.defer();

      var user = self.user;
      user.set('username', data.email);
      user.set('name', data.name);
      user.set('email', data.email);

      user.save(null, {
        success: function (user) {
          self.user = user;
          d.resolve(self.user);
        },
        error: function (user, error) {
          $ionicPopup.alert({
            title: 'Update error',
            subtitle: error.message
          });
          d.reject();
        }
      });

			return d.promise;
		}

	};

	return self;
})
;

