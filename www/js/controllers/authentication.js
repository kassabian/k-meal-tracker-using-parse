var app = angular.module('mealtrack.controllers.authentication', []);

/*********************************************************************
 * LoginCtrl
 *********************************************************************/
app.controller('LoginCtrl', function ($scope, $state, AuthService) {

	$scope.formData = {
		"email": "",
		"password": ""
	};

	$scope.login = function (loginForm) {
    if(loginForm.$valid) {
      AuthService.login($scope.formData.email, $scope.formData.password).then(function() {
        $state.go("tab.meals");
      });
    }
	};

});

/*********************************************************************
 * SignupCtrl
 *********************************************************************/
app.controller('SignupCtrl', function ($scope, $state, AuthService) {

	$scope.formData = {
		"name": "",
		"email": "",
		"password": ""
	};

	$scope.signup = function (signupForm) {
		if(signupForm.$valid) {
      console.log("SignupCtrl::signup");
      //TODO
      AuthService.signup($scope.formData.email, $scope.formData.name, $scope.formData.password)
        .then(function() {
          $state.go("tab.meals");
        });

    }
	};

});
